use std::fs;
use std::time::{Duration, SystemTime};
use std::path::PathBuf;
use std::collections::BTreeMap;

use eyre::{eyre, Result, WrapErr};
use tokio::time;
use structopt::StructOpt;
use serde_json::json;
use tracing::Level;
use tracing_subscriber::prelude::*;
use serenity::model::id::{GuildId, ChannelId, MessageId, UserId};
use serenity::model::channel::{ChannelType};

use worldstatus::sql::{Change, Store};

#[derive(StructOpt)]
struct Opts {
	#[structopt(short, long, default_value = "worldstatus.sqlite")]
	db_path: PathBuf,
	#[structopt(short = "i", long, default_value = "5s",
			parse(try_from_str = humantime::parse_duration))]
	update_interval: time::Duration,
	#[structopt(long)]
	guild_id: GuildId,
	#[structopt(long)]
	bot_token_file: PathBuf,
	#[structopt(long)]
	only_dc: Option<String>,
	#[structopt(long)]
	show_changes: bool,
}

struct App {
	client: serenity::http::Http,
	store: Store,
	update_interval: Duration,
	user_id: UserId,
	guild_id: GuildId,
	channels: BTreeMap<String, ChannelId>,
	only_dc: Option<String>,
}

#[tokio::main]
async fn main() -> Result<()> {
	let filter = tracing_subscriber::filter::Targets::new()
		.with_target("worldstatus", Level::TRACE)
		.with_target("worldstatus-reporter", Level::TRACE);
	use tracing_subscriber::fmt::format::FmtSpan;
	let subscriber = tracing_subscriber::fmt()
		.pretty()
		.with_thread_names(true)
		// enable everything
		.with_max_level(tracing::Level::INFO)
		.with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
		.finish()
		.with(filter);
	tracing::subscriber::set_global_default(subscriber)?;

	let opts = Opts::from_args();

	if opts.show_changes {
		let mut store = Store::new(&opts.db_path)?;
		let changes = store.get_changes()?;
		dbg!(changes);
		return Ok(());
	}

	let bot_token = fs::read_to_string(&opts.bot_token_file)?;
	let client =
		serenity::http::Http::new(bot_token.trim());
	let user_id = client.get_current_user().await?.id;
	let mut app = App {
		client: client,
		store: Store::new(&opts.db_path)?,
		update_interval: opts.update_interval,
		user_id: user_id,
		guild_id: opts.guild_id,
		channels: BTreeMap::new(),
		only_dc: opts.only_dc,
	};

	app.run().await;
	Ok(())
}

impl App {
	async fn run(&mut self) {
		loop {
			if let Err(e) = self.update().await {
				eprintln!("update failed: {:?}", e); // TODO how do you report errors again
			}

			time::sleep(self.update_interval).await;
		}
	}

	#[tracing::instrument(skip(self), level = "trace")]
	async fn update(&mut self) -> Result<()> {
		self.channels = self.client.get_channels(self.guild_id).await?
			.into_iter().map(|mut c| {
				c.name.make_ascii_lowercase();
				(c.name, c.id)
			}).collect();
		let changes = self.store.get_changes()?;
		for c in &changes {
			if let Some(only_dc) = &self.only_dc {
				if only_dc != &c.dc {
					continue;
				}
			}

			if let Err(e) = self.publish_update(c).await {
				eprintln!("update for {} failed: {:?}", c.world, e);
			}
		}
		Ok(())
	}

	#[tracing::instrument(skip(self))]
	async fn publish_update(&mut self, c: &Change) -> Result<()> {
		const STATUS_EMOTE_AVAILABLE: &str = "white_check_mark";
		const STATUS_EMOTE_UNAVAILABLE: &str = "no_entry_sign";
		const STATUS_LABEL_AVAILABLE: &str = "available";
		const STATUS_LABEL_UNAVAILABLE: &str = "unavailable";

		fn msg_available(world: &str) -> Result<String>{
			Ok(format!(":{}: **{}**: {}!",
				STATUS_EMOTE_AVAILABLE, world, STATUS_LABEL_AVAILABLE))
		}

		fn msg_unavailable(world: &str, t: SystemTime) -> Result<String> {
			let t = t.duration_since(SystemTime::UNIX_EPOCH)
				.map_err(|_| eyre!("system time before unix epoch wtf"))?
				.as_secs();
			Ok(format!("**{}** was {}.\n:{}: {} as of <t:{}:t>",
				world, STATUS_LABEL_AVAILABLE, STATUS_EMOTE_UNAVAILABLE,
				STATUS_LABEL_UNAVAILABLE, t))
		}

		fn msg_never_available(world: &str) -> Result<String> {
			Ok(format!(":{}: **{}**: {}.\n",
				STATUS_EMOTE_UNAVAILABLE, world, STATUS_LABEL_UNAVAILABLE))
		}

		let msg;
		let msg_id;
		let msg_id_old: Option<MessageId> = c.msg_id_old.map(Into::into);
		if c.available_new {
			msg = msg_available(&c.world)?;
			msg_id = Some(self.post_msg(&c.dc, &c.world, &msg).await?);
		} else if let Some(msg_id_old) = msg_id_old {
			msg = msg_unavailable(&c.world, c.time_new)?;
			self.update_msg(&c.dc, &c.world, msg_id_old, &msg).await?;
			// TODO: should fall back to next case when the message
			// doesn't exist anymore
			msg_id = Some(msg_id_old);
		} else {
			// status change to unavailable but we never announced it
			// as available. kind of an edge case huh.
			msg = msg_never_available(&c.world)?;
			msg_id = Some(self.post_msg(&c.dc, &c.world, &msg).await?);
		}

		if let Some(msg_id) = msg_id {
			self.store.record_published(&c.world, c.status_id, msg_id.into())?;
		}

		Ok(())
	}

	#[tracing::instrument(skip(self), level = "trace")]
	async fn post_msg(&mut self, dc: &str, world: &str,
			msg: &str) -> Result<MessageId> {
		let channel_id = self.get_or_create_channel(dc, world).await?;

		// TODO: should delete channel from cache if this fails because
		// the channel doesnt exist, and retry
		Ok(self.client.send_message(channel_id, Vec::new(), &json!({
			"content": msg,
			"allowed_mentions": { "parse": [] },
		})).await?.id)
	}

	#[tracing::instrument(skip(self), level = "trace")]
	async fn update_msg(&mut self, dc: &str, world: &str, msg_id: MessageId, msg: &str) -> Result<()> {
		let channel_id = self.get_or_create_channel(dc, world).await?;

		self.client.edit_message(channel_id, msg_id, &json!({
			"content": msg,
			"allowed_mentions": { "parse": [] },
		}), Vec::new()).await?;
		Ok(())
	}

	async fn get_or_create_channel(&mut self, dc: &str, world: &str)
			-> Result<ChannelId> {
		let mut dc_lower = dc.to_string();
		dc_lower.make_ascii_lowercase();
		let mut world_lower = world.to_string();
		world_lower.make_ascii_lowercase();
		if let Some(&channel_id) = self.channels.get(&world_lower) {
			return Ok(channel_id);
		}

		let category_id = match self.channels.get(&dc_lower) {
			Some(&category_id) => category_id,
			None => {
				let category_id = self.create_category(dc).await?;
				self.channels.insert(dc_lower, category_id);
				category_id
			}
		};

		let channel_id = self.create_channel(category_id, world).await?;
		self.channels.insert(world_lower, channel_id);
		Ok(channel_id)
	}

	fn make_channel_permission_overwrites(&self) -> serde_json::Value {
		json!([
			{
				"id": self.user_id,
				"type": 1, // user
				"allow":
					0x0000000800u64 // SEND_MESSAGES
					| 0x0800000000u64 // CREATE_PUBLIC_THREADS
					| 0x1000000000u64 // CREATE_PRIVATE_THREADS
					| 0x4000000000u64 // SEND_MESSAGES_IN_THREADS
					| 0x0080000000u64 // USE_APPLICATION_COMMANDS
					| 0x0000000040u64 // ADD_REACTIONS
			},
			{
				"id": self.guild_id, // = @everyone
				"type": 0, // role
				"deny":
					0x0000000800u64 // SEND_MESSAGES
					| 0x0800000000u64 // CREATE_PUBLIC_THREADS
					| 0x1000000000u64 // CREATE_PRIVATE_THREADS
					| 0x4000000000u64 // SEND_MESSAGES_IN_THREADS
					| 0x0080000000u64 // USE_APPLICATION_COMMANDS
					| 0x0000000040u64 // ADD_REACTIONS
			},
		])
	}

	#[tracing::instrument(skip(self))]
	async fn create_category(&mut self, dc: &str) -> Result<ChannelId> {
		let args = json!({
			"name": dc,
			"type": channel_type_to_u8(ChannelType::Category),
			"permission_overwrites":
				self.make_channel_permission_overwrites(),
		});
		let id = self.client.create_channel(self.guild_id, &args, None).await
			.wrap_err("failed to create category")?.id;
		Ok(id)
	}

	#[tracing::instrument(skip(self))]
	async fn create_channel(&mut self, category_id: ChannelId, world: &str)
			-> Result<ChannelId> {
		let args = json!({
			"name": world,
			"type": channel_type_to_u8(ChannelType::Text),
			"parent_id": category_id,
			"permission_overwrites":
				self.make_channel_permission_overwrites(),
		});
		let id = self.client.create_channel(self.guild_id, &args, None).await
			.wrap_err("failed to create channel")?.id;
		self.channels.insert(world.to_string(), id);
		Ok(id)
	}
}

fn channel_type_to_u8(channel_type: ChannelType) -> u8 {
	for i in 0..=255 {
		if ChannelType::from(i) == channel_type {
			return i;
		}
	}

	panic!("what even are enums");
}
