use std::time::Duration;
use std::path::PathBuf;

use eyre::Result;
use tokio::{time, task};
use structopt::StructOpt;
use rand::prelude::*;
use tracing::Level;
use tracing_subscriber::prelude::*;

use worldstatus::sql::Store;

#[derive(StructOpt)]
struct Opts {
	#[structopt(short, long, default_value = "worldstatus.sqlite")]
	db_path: PathBuf,
	#[structopt(short, long, default_value = "30s",
			parse(try_from_str = humantime::parse_duration))]
	update_interval: time::Duration,
}

struct App {
	client: reqwest::Client,
	store: Store,
	update_interval: Duration,
}

#[tokio::main]
async fn main() -> Result<()> {
	let filter = tracing_subscriber::filter::Targets::new()
		.with_target("worldstatus", Level::TRACE)
		.with_target("worldstatus-inserter", Level::TRACE);
	use tracing_subscriber::fmt::format::FmtSpan;
	let subscriber = tracing_subscriber::fmt()
		.pretty()
		.with_thread_names(true)
		.with_max_level(tracing::Level::INFO)
		.with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
		.finish()
		.with(filter);
	tracing::subscriber::set_global_default(subscriber)?;

	let opts = Opts::from_args();
	let client = reqwest::ClientBuilder::new().build()?;
	let store = Store::new(&opts.db_path)?;
	let update_interval = opts.update_interval;
	let mut app = App { client, store, update_interval };
	app.run().await;
	Ok(())
}

impl App {
	async fn run(&mut self) {
		let mut interval = time::interval(self.update_interval);
		interval.set_missed_tick_behavior(time::MissedTickBehavior::Skip);
		interval.tick().await; // consume immediately-ready tick
		loop {
			let update = self.update();
			let result = tokio::select! {
				r = update => r,
				_ = interval.tick() => {
					tracing::warn!("update_timeout");
					continue;
				}
			};

			if let Err(error) = result {
				tracing::error!(?error, "update failed"); // TODO how do you report errors again
			}

			interval.tick().await;
		}
	}

	#[tracing::instrument(skip(self), level = "info")]
	async fn update(&mut self) -> Result<()> {
		let resp = self.client
			.get("https://na.finalfantasyxiv.com/lodestone/worldstatus/")
			.send()
			.await?;
		let body = resp.text().await?;
		let mut dcs = worldstatus::extract_availability(&body)?;

		// if we hit the timeout and get canceled, it'd be good if we
		// update something else first the next time.
		dcs.shuffle(&mut rand::thread_rng());

		for (dc, mut worlds) in dcs {
			worlds.shuffle(&mut rand::thread_rng());

			for (world, availability) in worlds {
				if let Some(availability) = availability {
					let available =
						availability == worldstatus::Availability::Available;
					if self.store.record_status(&dc, &world, available)? {
						let label = format!("{dc}/{world}:");
						eprintln!(
                            "  {label:25} {availability:?} ({available})");
					}
				}
			}

			// There's not actually any async stuff happening above, so
			// if the sqlite queries take a long time, tokio can't
			// actually cancel this task if we don't do something like
			// this:
			task::yield_now().await;
		}
		Ok(())
	}
}
