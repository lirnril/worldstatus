use std::thread;
use std::time::Duration;
use std::collections::{HashMap, hash_map::Entry};
use eyre::Result;

#[tokio::main]
async fn main() -> Result<()> {
	let client = reqwest::ClientBuilder::new().build()?;
	let mut availability_by_world = HashMap::new();
	loop {
		let resp = client
			.get("https://na.finalfantasyxiv.com/lodestone/worldstatus/")
			.send()
			.await?;
		let body = resp.text().await?;
		let dcs = worldstatus::extract_availability(&body)?;
		for (dc_name, availabilities) in dcs {
			eprintln!("dc: {}", dc_name);
			for (name, ref available) in availabilities {
				match availability_by_world.entry(name) {
					Entry::Occupied(mut entry) => {
						let v = entry.get_mut();
						if v != available {
							*v = available.clone();
							eprintln!("world={} available={:?}", entry.key(), available);
						}
					}
					Entry::Vacant(entry) => {
						eprintln!("world={} available={:?}", entry.key(), available);
						entry.insert(available.clone());
					}
				}
			}
		}

		thread::sleep(Duration::from_secs(5));
	}
}
