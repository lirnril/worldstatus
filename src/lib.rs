pub mod extract;
pub mod sql;

pub use eyre::Result;

pub use extract::extract_availability;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub enum Availability {
	Available,
	Unavailable,
	Maintenance,
}
