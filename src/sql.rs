use std::path::Path;
use std::time::{Duration, SystemTime};

use eyre::{eyre, Result, WrapErr};
use rusqlite::named_params;
use tracing::{instrument, span, Level};

pub struct Store {
	conn: rusqlite::Connection,
}

#[derive(Debug)]
pub struct Change {
	pub status_id: u64,
	pub time_new: SystemTime,
	pub time_old: Option<SystemTime>,
	pub world: String,
	pub dc: String,
	pub msg_id_old: Option<u64>,
	pub available_new: bool,
	pub available_old: Option<bool>,
}

fn from_unix(secs: u64) -> SystemTime {
	SystemTime::UNIX_EPOCH + Duration::from_secs(secs)
}

impl Store {
	pub fn new(db_path: impl AsRef<Path>) -> Result<Store> {
		let conn = rusqlite::Connection::open(db_path.as_ref())?;
		conn.execute_batch(PRELUDE)?;
		Ok(Store { conn })
	}

	#[instrument(skip(self), level = "trace")]
	fn get_prev(&self, world: &str) -> Result<Option<(u64, i32)>> {
		use rusqlite::OptionalExtension;
		let f = |r: &rusqlite::Row| -> Result<(u64, i32), _> {
			Ok((r.get(0)?, r.get(1)?))
		};
		let r = self.conn.query_row(r#"
				select s.id, s.character_creation_available
				from statuses s
				left join world_names w on w.id = s.world
				where w.name = ?
					and not exists (
						select n.id from statuses n
						where n.world = s.world
							and n.time_first_seen > s.time_first_seen
					)
			"#, [world], f).optional()?;

		Ok(r)
	}

	#[instrument(skip(self), level = "trace")]
	pub fn record_status(&self, dc: &str, world: &str, available: bool) -> Result<bool> {
		let prev = self.get_prev(world)?;

		let prev_changed = prev.map(|(id, prev_available)|
			(id, (prev_available != 0) != available));

		let now = SystemTime::UNIX_EPOCH.elapsed()
			.map_err(|_| eyre!("system time before unix epoch wtf"))?
			.as_secs();

		match prev_changed {
			Some((id, false)) => {
				let span = span!(Level::TRACE, "unchanged", ?available);
				let _enter = span.enter();
				self.conn.execute(r#"
						update statuses
						set time_last_seen = :now
						where id = :id;
					"#, named_params! {
						":now": now,
						":id": id,
					}).wrap_err("failed to update existing status")?;
				Ok(false)
			}
			None | Some((_, true)) => {
				let span = span!(Level::TRACE, "changed", ?available);
				let _enter = span.enter();
				self.conn.execute(r#"
						insert or ignore into dc_names (name)
						values (?);
				"#, [dc]).wrap_err("failed to insert dc name")?;
				self.conn.execute(r#"
						insert or ignore into world_names (name, dc)
						values (
							:world,
							(select id from dc_names where name = :dc)
						);
				"#, named_params! {
					":world": world,
					":dc": dc,
				}).wrap_err_with(|| format!(
					"failed to insert world name world={} dc={}",
					world, dc))?;
				let span = span!(Level::TRACE, "insert", ?available);
				let _enter = span.enter();
				self.conn.execute(r#"
						insert into statuses (
							time_first_seen, time_last_seen, world,
							character_creation_available
						) values (
							:now,
							:now,
							(select id from world_names where name = :world),
							:available
						)
					"#, named_params! {
						":now": now,
						":world": world,
						":available": if available { 1 } else { 0 },
					})?;
				Ok(true)
			}
		}
	}

	#[instrument(skip(self), level = "trace")]
	pub fn get_changes(&mut self) -> Result<Vec<Change>> {
		let mut stmt = self.conn.prepare(r#"
				select
					s.id,

					s.time_first_seen,
					m.time_last_seen,
					w.name,
					d.name,

					q.msg,
					s.character_creation_available,
					m.character_creation_available
				from world_names w
				join statuses s on w.id = s.world
					and not exists(
						select * from statuses n
						where n.world = s.world
							and n.time_first_seen > s.time_first_seen)
					and not exists(
						select * from published p
						where p.status = s.id)
				left join published q
					on q.world = s.world
					and not exists(
						select * from published r
						where r.world = q.world
							and r.time > q.time)
				left join statuses m on m.id = q.status
				left join dc_names d on d.id = w.dc
			"#)?;

		let changes = stmt.query_map([], |r| Ok(Change {
				status_id: r.get(0)?,
				time_new: from_unix(r.get(1)?),
				time_old: r.get::<_, Option<u64>>(2)?.map(from_unix),
				world: r.get(3)?,
				dc: r.get(4)?,
				msg_id_old: r.get(5)?,
				available_new: r.get(6)?,
				available_old: r.get(7)?,
			}))?.collect::<Result<Vec<_>, _>>()?;

		Ok(changes)
	}

	#[instrument(skip(self), level = "trace")]
	pub fn record_published(&self, world: &str, status_id: u64, msg_id: u64)
			-> Result<()> {
		let now = SystemTime::UNIX_EPOCH.elapsed()
			.map_err(|_| eyre!("system time before unix epoch wtf"))?
			.as_secs();
		self.conn.execute( r#"
				insert into published (time, world, status, msg)
				values (?, (select id from world_names where name = ?), ?, ?)
			"#, [&now as &dyn rusqlite::ToSql, &world, &status_id, &msg_id])?;
		Ok(())
	}
}

const PRELUDE: &'static str = r#"

	pragma foreign_keys = on;

	create table if not exists world_names (
		id   integer primary key not null,
		name text unique not null,
		dc   integer not null,

		foreign key (dc) references dc_names(id)
	);

	create table if not exists dc_names (
		id integer primary key not null,
		name text unique not null
	);

	create table if not exists statuses (
		id                           integer primary key not null,
		time_first_seen              integer not null,
		time_last_seen               integer not null,
		world                        integer not null,
		character_creation_available integer not null,

		foreign key (world) references world_names(id)
	);

	create index if not exists statuses_world_time on statuses (
		world asc,
		time_first_seen asc
	);

	create table if not exists published (
		id      integer primary key not null,
		time    integer not null,
		world   integer not null,
		status  integer not null,
		msg     integer not null,
		--channel integer not null,

		foreign key (status) references statuses(id),
		foreign key (world) references world_names(id)
	);

	create index if not exists published_world_time on published (
		world asc,
		time asc
	);

	create index if not exists published_status on published (
		status asc
	);

"#;
