use eyre::eyre;
use crate::{Availability::{self, *}, Result};

fn selector(s: &str) -> Result<scraper::Selector> {
	scraper::Selector::parse(s).map_err(|e| eyre!("{:?}", e))
}

const SELECTOR_DC: &'static str = ".world-dcgroup__item";
const SELECTOR_DC_NAME: &'static str = ".world-dcgroup__header";
const SELECTOR_ITEM: &'static str = ".world-list__item";
const SELECTOR_WORLD: &'static str = ".world-list__world_name > p";
const SELECTOR_STATUS: &'static str = ".world-list__status_icon > i";
const SELECTOR_AVAILABILITY: &'static str = ".world-list__create_character > i";

const CLASS_AVAILABLE: &'static str = "world-ic__available";
const CLASS_UNAVAILABLE: &'static str = "world-ic__unavailable";
const CLASS_PARTIAL_MAINTENANCE: &'static str = "world-ic__2";
const CLASS_MAINTENANCE: &'static str = "world-ic__3";

/// Takes the html content of the lodestone world status page at
/// https://na.finalfantasyxiv.com/lodestone/worldstatus/ and whether
/// character creation is available for each server, if detected.
pub fn extract_availability(s: &str)
		-> Result<Vec<(String, Vec<(String, Option<Availability>)>)>> {
	let doc = scraper::Html::parse_document(&s);
	let dcs = selector(SELECTOR_DC)?;
	let dc_names = selector(SELECTOR_DC_NAME)?;
	let items = selector(SELECTOR_ITEM)?;
	let names = selector(SELECTOR_WORLD)?;
	let statuses = selector(SELECTOR_STATUS)?;
	let availabilities = selector(SELECTOR_AVAILABILITY)?;

	let dcs = doc.select(&dcs).filter_map(|dc_ref| {
		let dc_name = dc_ref.select(&dc_names).next()
			.and_then(|r| r.text().next())?
			.into();

		let availabilities = dc_ref.select(&items).filter_map(|item_ref| {
			let name = item_ref.select(&names).next()
				.and_then(|r| r.text().next())?
				.into();
			let availability = item_ref.select(&statuses).next()
				.and_then(|v| v.value().classes().filter_map(|c| match c {
						CLASS_MAINTENANCE | CLASS_PARTIAL_MAINTENANCE =>
							Some(Maintenance),
						_ => None
					}).next()
				.or_else(|| item_ref.select(&availabilities).next()
					.and_then(|v| v.value().classes().filter_map(|c| match c {
							CLASS_AVAILABLE => Some(Available),
							CLASS_UNAVAILABLE => Some(Unavailable),
							_ => None
						}).next())));
			Some((name, availability))
		}).collect();

		Some((dc_name, availabilities))
	}).collect();


	Ok(dcs)
}

#[cfg(test)]
mod test {
	use super::*;

	#[test]
	fn test_static() {
		let availabilities = extract_availability(SAMPLE_HTML).unwrap();
		let availabilities: Vec<_> = availabilities.iter()
			.map(|(n, a)| (&n[..], &a[..])).collect();
		assert_eq!(availabilities, &[
			("Primal", &[
				("Behemoth".to_string(), Some(Unavailable)),
				("Excalibur".to_string(), Some(Available)),
				("Exodus".to_string(), Some(Available)),
				("Famfrit".to_string(), Some(Available)),
				("Hyperion".to_string(), Some(Available)),
				("Lamia".to_string(), Some(Available)),
				("Leviathan".to_string(), Some(Available)),
				("Ultros".to_string(), Some(Available)),
			] as &[_]),
			("Crystal", &[
				("Balmung".to_string(), Some(Maintenance)),
				("Brynhildr".to_string(), Some(Maintenance)),
				("Coeurl".to_string(), Some(Maintenance)),
				("Diabolos".to_string(), Some(Maintenance)),
				("Goblin".to_string(), Some(Maintenance)),
				("Malboro".to_string(), Some(Maintenance)),
				("Mateus".to_string(), Some(Maintenance)),
				("Zalera".to_string(), Some(Maintenance)),
			]),
		]);
	}

	const SAMPLE_HTML: &'static str = r#"<li class="world-dcgroup__item">
		<h2 class="world-dcgroup__header">Primal</h2>
		<ul>
			<li class="item-list">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__1 js__tooltip" data-tooltip="
							Online">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Behemoth</p>
					</div>
					<div class="world-list__world_category">
						<p>Standard</p>
					</div>
					<div class="world-list__create_character">
						<i class="world-ic__unavailable js__tooltip" data-tooltip="Creation of New Characters Unavailable"></i>
					</div>
				</div>
			</li>
			<li class="item-list">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__1 js__tooltip" data-tooltip="
							Online">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Excalibur</p>
					</div>
					<div class="world-list__world_category">
						<p>Standard</p>
					</div>
					<div class="world-list__create_character">
						<i class="world-ic__available js__tooltip" data-tooltip="Creation of New Characters Available"></i>
					</div>
				</div>
			</li>
			<li class="item-list">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__1 js__tooltip" data-tooltip="
							Online">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Exodus</p>
					</div>
					<div class="world-list__world_category">
						<p>Standard</p>
					</div>
					<div class="world-list__create_character">
						<i class="world-ic__available js__tooltip" data-tooltip="Creation of New Characters Available"></i>
					</div>
				</div>
			</li>
			<li class="item-list">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__1 js__tooltip" data-tooltip="
							Online">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Famfrit</p>
					</div>
					<div class="world-list__world_category">
						<p>Standard</p>
					</div>
					<div class="world-list__create_character">
						<i class="world-ic__available js__tooltip" data-tooltip="Creation of New Characters Available"></i>
					</div>
				</div>
			</li>
			<li class="item-list">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__1 js__tooltip" data-tooltip="
							Online">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Hyperion</p>
					</div>
					<div class="world-list__world_category">
						<p>Standard</p>
					</div>
					<div class="world-list__create_character">
						<i class="world-ic__available js__tooltip" data-tooltip="Creation of New Characters Available"></i>
					</div>
				</div>
			</li>
			<li class="item-list">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__1 js__tooltip" data-tooltip="
							Online">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Lamia</p>
					</div>
					<div class="world-list__world_category">
						<p>Standard</p>
					</div>
					<div class="world-list__create_character">
						<i class="world-ic__available js__tooltip" data-tooltip="Creation of New Characters Available"></i>
					</div>
				</div>
			</li>
			<li class="item-list">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__1 js__tooltip" data-tooltip="
							Online">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Leviathan</p>
					</div>
					<div class="world-list__world_category">
						<p>Standard</p>
					</div>
					<div class="world-list__create_character">
						<i class="world-ic__available js__tooltip" data-tooltip="Creation of New Characters Available"></i>
					</div>
				</div>
			</li>
			<li class="item-list">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__1 js__tooltip" data-tooltip="
							Online">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Ultros</p>
					</div>
					<div class="world-list__world_category">
						<p>Standard</p>
					</div>
					<div class="world-list__create_character">
						<i class="world-ic__available js__tooltip" data-tooltip="Creation of New Characters Available"></i>
					</div>
				</div>
			</li>
		</ul>
	</li>

	<li class="world-dcgroup__item">
		<h2 class="world-dcgroup__header">Crystal</h2>
		<ul>
			<li class="item-list
				world-list__maintenance
				">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__3 js__tooltip" data-tooltip="
							Maintenance
							">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Balmung</p>
					</div>
					<div class="world-list__world_category">
						<p>--</p>
					</div>
					<div class="world-list__create_character">
						<p>--</p>
					</div>
				</div>
			</li>
			<li class="item-list
				world-list__maintenance
				">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__3 js__tooltip" data-tooltip="
							Maintenance
							">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Brynhildr</p>
					</div>
					<div class="world-list__world_category">
						<p>--</p>
					</div>
					<div class="world-list__create_character">
						<p>--</p>
					</div>
				</div>
			</li>
			<li class="item-list
				world-list__maintenance
				">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__3 js__tooltip" data-tooltip="
							Maintenance
							">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Coeurl</p>
					</div>
					<div class="world-list__world_category">
						<p>--</p>
					</div>
					<div class="world-list__create_character">
						<p>--</p>
					</div>
				</div>
			</li>
			<li class="item-list
				world-list__maintenance
				">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__3 js__tooltip" data-tooltip="
							Maintenance
							">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Diabolos</p>
					</div>
					<div class="world-list__world_category">
						<p>--</p>
					</div>
					<div class="world-list__create_character">
						<p>--</p>
					</div>
				</div>
			</li>
			<li class="item-list
				world-list__maintenance
				">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__3 js__tooltip" data-tooltip="
							Maintenance
							">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Goblin</p>
					</div>
					<div class="world-list__world_category">
						<p>--</p>
					</div>
					<div class="world-list__create_character">
						<p>--</p>
					</div>
				</div>
			</li>
			<li class="item-list
				world-list__maintenance
				">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__3 js__tooltip" data-tooltip="
							Maintenance
							">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Malboro</p>
					</div>
					<div class="world-list__world_category">
						<p>--</p>
					</div>
					<div class="world-list__create_character">
						<p>--</p>
					</div>
				</div>
			</li>
			<li class="item-list
				world-list__maintenance
				">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__3 js__tooltip" data-tooltip="
							Maintenance
							">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Mateus</p>
					</div>
					<div class="world-list__world_category">
						<p>--</p>
					</div>
					<div class="world-list__create_character">
						<p>--</p>
					</div>
				</div>
			</li>
			<li class="item-list
				world-list__maintenance
				">
				<div class="world-list__item">
					<div class="world-list__status_icon">
						<i class="world-ic__3 js__tooltip" data-tooltip="
							Maintenance
							">
						</i>
					</div>
					<div class="world-list__world_name">
						<p>Zalera</p>
					</div>
					<div class="world-list__world_category">
						<p>--</p>
					</div>
					<div class="world-list__create_character">
						<p>--</p>
					</div>
				</div>
			</li>
		</ul>
	</li>
	"#;
}

