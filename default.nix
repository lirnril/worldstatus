{ pkgs ? import ./nix/nixpkgs.nix }:

let
  cargoNix = import ./Cargo.nix { inherit pkgs; };
  uniqueConcatAttrs = f: a: pkgs.lib.unique (builtins.concatLists (map f a));
in {
  inherit (cargoNix.rootCrate) build;
  shell = pkgs.mkShell {
    buildInputs = [pkgs.sqlite-interactive] ++ uniqueConcatAttrs
      (d: d.buildInputs)
      cargoNix.rootCrate.build.completeDeps;
    nativeBuildInputs = uniqueConcatAttrs
      (d: d.nativeBuildInputs)
      cargoNix.rootCrate.build.completeDeps;
  };
}
